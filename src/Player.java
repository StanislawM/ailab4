import java.awt.*;
import java.util.Random;

abstract class Player {
    int pawns = 9;
    int newPawns = 9;
    PlayerEnum playerEnum;
    Color color;
    Random random = new Random();
    boolean canRemove = false;
    int lastPosition = -1;

    public Player() {
    }

    @Override
    public String toString() {
        return "Pawns: " + pawns + " " + playerEnum.toString();
    }

    public void minMax(View view, Player nextPlayer) {
        sleep();
        switch (view.gameStage) {
            case FIRST_STAGE:
                firstStage(view, nextPlayer);
                break;
            case SECOND_STAGE:
                secondStage(view, nextPlayer);
                break;
            case END_GAME:
                break;
        }
    }

    protected void firstStage(View view, Player nextPlayer) {
        if (!this.canRemove) {
            GameButton position = null;
            position = findPosition(view, position);
            runEngine(view, nextPlayer, position);
        } else {
            removeNextPlayerPawn(view, nextPlayer);
        }
        if (view.playerOne.newPawns + view.playerTwo.newPawns <= 0) {
            view.gameStage = GameStage.SECOND_STAGE;
            System.out.println("Second");
        }
    }

    private void secondStage(View view, Player nextPlayer) {
        GameButton position = null;
        if (this.canRemove) {
            removeNextPlayerPawn(view, nextPlayer);
        } else if ((position = canMotion(view)) != null) {
            runEngine(view, nextPlayer, position);
        } else {
            while (position == null
                    || position.getPlayer() == null
                    || (position.isPlayerSet()
                    && position.selectedPlayer() != this.playerEnum)) {
                position = view.gameButtonMap.get(random.nextInt(view.gameButtonMap.size()));
            }
            this.lastPosition = position.id;
            position.doClick();
            this.minMax(view, nextPlayer);
        }
    }

    private GameButton canMotion(View view) {
        GameButton position = null;
        if (this.lastPosition < 0) {
            return null;
        } else {
            if (view.selectedPlayer.pawns < 3) {
                view.gameStage = GameStage.END_GAME;
            }
            if (view.selectedPlayer.pawns == 3) {
                position = findPosition(view, position);
                return position;
            } else {
                for (int i : view.buttonEvent.motionPosition.get(this.lastPosition)) {
                    if (!view.gameButtonMap.get(i).isPlayerSet()) {
                        return view.gameButtonMap.get(i);
                    }
                }
            }
            return null;
        }
    }

    private GameButton findPosition(View view, GameButton position) {
        for (GameButton button : view.gameButtonMap.values()) {
            if (position == null && canRemovePawn(view, button.id) && !button.isPlayerSet()) {
                position = button;
            }
        }
        if (position == null) {
            while (position == null || position.isPlayerSet()) {
                position = view.gameButtonMap.get(random.nextInt(view.gameButtonMap.size()));
            }
        }
        return position;
    }

    public boolean canRemovePawn(View view, int id) {
        int counter = 0;
        for (int[] gameTab : view.buttonEvent.threesPosition.get(id)) {
            for (int gamePosition : gameTab) {
                if (gamePosition != id) {
                    if (view.gameButtonMap.get(gamePosition).getPlayer() != null
                            && view.gameButtonMap.get(gamePosition).selectedPlayer() == this.playerEnum) {
                        counter++;
                    }
                }
            }
        }
        return counter >= 2;
    }

    private void runEngine(View view, Player nextPlayer, GameButton position) {
        position.doClick();
        playerTour(view, position.id, nextPlayer);
    }

    private void playerTour(View view, int id, Player nextPlayer) {
        if (!view.buttonEvent.canRemovePawn(id)) {
            nextPlayer.minMax(view, this);
        } else {
            this.canRemove = true;
            this.minMax(view, nextPlayer);
        }
    }

    private void removeNextPlayerPawn(View view, Player nextPlayer) {
        GameButton position = null;
        while (position == null
                || position.getPlayer() == null
                || (position.isPlayerSet()
                && position.selectedPlayer() == this.playerEnum)) {
            position = view.gameButtonMap.get(random.nextInt(view.gameButtonMap.size()));
        }

        runEngine(view, nextPlayer, position);
    }

    private void sleep() {
        try {
            Thread.sleep(300);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
