import javax.swing.*;
import java.awt.*;
import java.util.HashMap;
import java.util.Map;

class View {
    Player playerOne = new PlayerOne();
    Player playerTwo = new PlayerTwo();
    Player selectedPlayer = playerOne;
    GameStage gameStage = GameStage.FIRST_STAGE;
    ButtonEvent buttonEvent = new ButtonEvent(this);
    Map<Integer, GameButton> gameButtonMap = new HashMap<>();

    View() {
    }

    public void runView() {
        JFrame f = new JFrame();
        f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        GamePanel jPanel = new GamePanel();
        jPanel.setLayout(null);
        jPanel.setPreferredSize(new Dimension(600, 600));
        createButton(jPanel, 40, 40, 0);
        createButton(jPanel, 40, 290, 1);
        createButton(jPanel, 40, 540, 2);
        createButton(jPanel, 140, 140, 3);
        createButton(jPanel, 140, 290, 4);
        createButton(jPanel, 140, 440, 5);
        createButton(jPanel, 240, 240, 6);
        createButton(jPanel, 240, 290, 7);
        createButton(jPanel, 240, 340, 8);
        createButton(jPanel, 290, 40, 9);
        createButton(jPanel, 290, 140, 10);
        createButton(jPanel, 290, 240, 11);
        createButton(jPanel, 290, 340, 12);
        createButton(jPanel, 290, 440, 13);
        createButton(jPanel, 290, 540, 14);
        createButton(jPanel, 340, 240, 15);
        createButton(jPanel, 340, 290, 16);
        createButton(jPanel, 340, 340, 17);
        createButton(jPanel, 440, 140, 18);
        createButton(jPanel, 440, 290, 19);
        createButton(jPanel, 440, 440, 20);
        createButton(jPanel, 540, 40, 21);
        createButton(jPanel, 540, 290, 22);
        createButton(jPanel, 540, 540, 23);
        f.add(jPanel);
        f.pack();
        f.setVisible(true);
    }

    private void createButton(GamePanel jPanel, int i, int i1, int id) {
        GameButton jButton = new GameButton(id);
        jButton.setBackground(Color.BLACK);
        jButton.setBounds(i, i1, 20, 20);
        jButton.setFont(new Font("Arial", Font.PLAIN, 10));
        this.gameButtonMap.put(id, jButton);
        jButton.addActionListener(buttonEvent);
        jPanel.add(jButton);
    }
}

class GamePanel extends JPanel {
    @Override
    public void paintComponent(Graphics g) {
        g.drawRect(50, 50, 500, 500);
        g.drawRect(150, 150, 300, 300);
        g.drawRect(250, 250, 100, 100);

        g.drawLine(50, 300, 250, 300);
        g.drawLine(300, 50, 300, 250);
        g.drawLine(300, 350, 300, 550);
        g.drawLine(350, 300, 550, 300);
    }
}

class GameButton extends JButton {
    private Player player = null;
    int id;

    public GameButton(int id) {
        this.id = id;
    }

    public void playerSelectPosition(Player player) {
        this.player = player;
        Color bg = player != null ? player.color : Color.BLACK;
        this.setBackground(bg);
    }

    public PlayerEnum selectedPlayer() {
        return this.player.playerEnum;
    }

    public boolean isPlayerSet() {
        return player != null;
    }

    public Player getPlayer() {
        return player;
    }
}