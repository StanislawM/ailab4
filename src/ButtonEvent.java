import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.HashMap;
import java.util.Map;

public class ButtonEvent implements ActionListener {
    public View view;
    public Map<Integer, int[][]> threesPosition = new HashMap<>();
    public Map<Integer, int[]> motionPosition = new HashMap<>();
    public int lastPosition = -1;

    ButtonEvent(View view) {
        this.view = view;
        loadThreesPosition();
        loadMotionPosition();
    }

    @Override
    public void actionPerformed(ActionEvent actionEvent) {
        switch (this.view.gameStage) {
            case FIRST_STAGE:
                firstStage(actionEvent);
                break;
            case SECOND_STAGE:
                secondStage(actionEvent);
                break;
            case END_GAME:
                break;
        }
    }

    public void firstStage(ActionEvent actionEvent) {
        if (this.view.selectedPlayer.playerEnum == PlayerEnum.PLAYER_ONE) {
            if (!this.view.selectedPlayer.canRemove) {
                gameEngine(actionEvent, this.view.playerTwo);
            } else {
                removeNextPlayerPawn(actionEvent, this.view.playerTwo);
            }
        } else {
            if (!this.view.selectedPlayer.canRemove) {
                gameEngine(actionEvent, this.view.playerOne);
            } else {
                removeNextPlayerPawn(actionEvent, this.view.playerOne);
            }
        }
        if (this.view.playerOne.newPawns + this.view.playerTwo.newPawns <= 0) {
            this.view.gameStage = GameStage.SECOND_STAGE;
            System.out.println("Second");
        }
    }

    public void secondStage(ActionEvent actionEvent) {
        if (this.view.selectedPlayer.playerEnum == PlayerEnum.PLAYER_ONE) {
            if (this.view.selectedPlayer.canRemove) {
                removeNextPlayerPawn(actionEvent, this.view.playerTwo);
            } else if (canMotion(actionEvent)) {
                gameEngine(actionEvent, this.view.playerTwo);
                this.view.selectedPlayer.lastPosition = -1;
            } else {
                if (((GameButton) actionEvent.getSource()).isPlayerSet()
                        && ((GameButton) actionEvent.getSource()).selectedPlayer() == PlayerEnum.PLAYER_ONE) {
                    this.view.selectedPlayer.lastPosition = ((GameButton) actionEvent.getSource()).id;
                }
            }
        } else {
            if (this.view.selectedPlayer.canRemove) {
                removeNextPlayerPawn(actionEvent, this.view.playerOne);
            } else if (canMotion(actionEvent)) {
                gameEngine(actionEvent, this.view.playerOne);
                this.view.selectedPlayer.lastPosition = -1;
            } else {
                if (((GameButton) actionEvent.getSource()).isPlayerSet()
                        && ((GameButton) actionEvent.getSource()).selectedPlayer() == PlayerEnum.PLAYER_TWO) {
                    this.view.selectedPlayer.lastPosition = ((GameButton) actionEvent.getSource()).id;
                }
            }
        }
    }

    public void gameEngine(ActionEvent actionEvent, Player player) {
        if (!((GameButton) actionEvent.getSource()).isPlayerSet()) {
            playerSelectPosition(actionEvent);
            playerTour(actionEvent, player);
        }
    }

    public void playerSelectPosition(ActionEvent actionEvent) {
        ((GameButton) actionEvent.getSource()).playerSelectPosition(this.view.selectedPlayer);
        this.view.selectedPlayer.newPawns--;
    }

    private void playerTour(ActionEvent actionEvent, Player player) {
        if (!canRemovePawn(((GameButton) actionEvent.getSource()).id)) {
            this.view.selectedPlayer = player;
        } else {
            this.view.selectedPlayer.canRemove = true;
        }
    }

    private boolean canMotion(ActionEvent actionEvent) {
        if (this.view.selectedPlayer.lastPosition < 0) {
            return false;
        } else {
            if (this.view.selectedPlayer.pawns < 3) {
                this.view.gameStage = GameStage.END_GAME;
            }
            if (this.view.selectedPlayer.pawns == 3) {
                if (!this.view.gameButtonMap.get(((GameButton) actionEvent.getSource()).id).isPlayerSet()) {
                    return changeColorAndReturnTrue();
                }
            } else {
                for (int i : this.motionPosition.get(this.view.selectedPlayer.lastPosition)) {
                    if (!this.view.gameButtonMap.get(i).isPlayerSet()
                            && i == ((GameButton) actionEvent.getSource()).id) {
                        return changeColorAndReturnTrue();
                    }
                }
            }
            return false;

        }
    }

    private boolean changeColorAndReturnTrue() {
        this.view.gameButtonMap.get(this.view.selectedPlayer.lastPosition).setBackground(Color.BLACK);
        this.view.gameButtonMap.get(this.view.selectedPlayer.lastPosition).playerSelectPosition(null);
        return true;
    }

    private void removeNextPlayerPawn(ActionEvent actionEvent, Player player) {
        if (((GameButton) actionEvent.getSource()).isPlayerSet()
                && ((GameButton) actionEvent.getSource()).selectedPlayer() != this.view.selectedPlayer.playerEnum) {
            ((GameButton) actionEvent.getSource()).setBackground(Color.BLACK);
            ((GameButton) actionEvent.getSource()).getPlayer().pawns--;
            ((GameButton) actionEvent.getSource()).playerSelectPosition(null);
            this.view.selectedPlayer.canRemove = false;
            this.view.selectedPlayer = player;
            System.out.println(this.view.playerOne.toString());
            System.out.println(this.view.playerTwo.toString());
        }
    }

    public boolean canRemovePawn(int id) {
        boolean canRemove;
        for (int[] gameTab : this.threesPosition.get(id)) {
            canRemove = true;
            for (int gamePosition : gameTab) {
                if (!this.view.gameButtonMap.get(gamePosition).isPlayerSet()
                        || this.view.selectedPlayer.playerEnum != this.view.gameButtonMap.get(gamePosition).selectedPlayer()) {
                    canRemove = false;
                }
            }
            if (canRemove) {
                return true;
            }
        }
        return false;
    }

    public void loadThreesPosition() {
        this.threesPosition.put(0, new int[][]{{0, 1, 2}, {0, 9, 21}});
        this.threesPosition.put(1, new int[][]{{0, 1, 2}, {1, 4, 7}});
        this.threesPosition.put(2, new int[][]{{0, 1, 2}, {2, 14, 23}});
        this.threesPosition.put(3, new int[][]{{3, 10, 18}, {3, 4, 5}});
        this.threesPosition.put(4, new int[][]{{1, 4, 7}, {3, 4, 5}});
        this.threesPosition.put(5, new int[][]{{3, 4, 5}, {5, 13, 20}});
        this.threesPosition.put(6, new int[][]{{6, 7, 8}, {6, 11, 15}});
        this.threesPosition.put(7, new int[][]{{1, 4, 7}, {6, 7, 8}});
        this.threesPosition.put(8, new int[][]{{6, 7, 8}, {8, 12, 17}});
        this.threesPosition.put(9, new int[][]{{0, 9, 21}, {9, 10, 11}});
        this.threesPosition.put(10, new int[][]{{3, 10, 18}, {9, 10, 11}});
        this.threesPosition.put(11, new int[][]{{6, 11, 15}, {9, 10, 11}});
        this.threesPosition.put(12, new int[][]{{8, 12, 17}, {12, 13, 14}});
        this.threesPosition.put(13, new int[][]{{5, 13, 20}, {12, 13, 14}});
        this.threesPosition.put(14, new int[][]{{2, 14, 23}, {12, 13, 14}});
        this.threesPosition.put(15, new int[][]{{15, 16, 17}, {6, 11, 15}});
        this.threesPosition.put(16, new int[][]{{16, 19, 22}, {15, 16, 17}});
        this.threesPosition.put(17, new int[][]{{15, 16, 17}, {8, 12, 17}});
        this.threesPosition .put(18, new int[][]{{18, 19, 20}, {3, 10, 18}});
        this.threesPosition.put(19, new int[][]{{18, 19, 20}, {16, 19, 22}});
        this.threesPosition.put(20, new int[][]{{18, 19, 20}, {5, 13, 20}});
        this.threesPosition.put(21, new int[][]{{0, 9, 21}, {21, 22, 23}});
        this.threesPosition.put(22, new int[][]{{16, 19, 22}, {21, 22, 23}});
        this.threesPosition.put(23, new int[][]{{2, 14, 23}, {21, 22, 23}});
    }

    public void loadMotionPosition() {
        this.motionPosition.put(0, new int[]{1, 9});
        this.motionPosition.put(1, new int[]{0, 2, 4});
        this.motionPosition.put(2, new int[]{1, 14});
        this.motionPosition.put(3, new int[]{4, 10});
        this.motionPosition.put(4, new int[]{1, 3, 5, 7});
        this.motionPosition.put(5, new int[]{4, 13});
        this.motionPosition.put(6, new int[]{7, 11});
        this.motionPosition.put(7, new int[]{4, 6, 8});
        this.motionPosition.put(8, new int[]{7, 12});
        this.motionPosition.put(9, new int[]{0, 10, 21});
        this.motionPosition.put(10, new int[]{3, 9, 11, 18});
        this.motionPosition.put(11, new int[]{6, 10, 15});
        this.motionPosition.put(12, new int[]{8, 13, 17});
        this.motionPosition.put(13, new int[]{5, 12, 14, 20});
        this.motionPosition.put(14, new int[]{2, 13, 23});
        this.motionPosition.put(15, new int[]{11, 16});
        this.motionPosition.put(16, new int[]{15, 17, 19});
        this.motionPosition.put(17, new int[]{12, 16});
        this.motionPosition.put(18, new int[]{10, 19});
        this.motionPosition.put(19, new int[]{16, 18, 20, 22});
        this.motionPosition.put(20, new int[]{13, 19});
        this.motionPosition.put(21, new int[]{9, 22});
        this.motionPosition.put(22, new int[]{19, 21, 23});
        this.motionPosition.put(23, new int[]{14, 22});
    }
}
