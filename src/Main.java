public class Main {
    public static void main(String[] args) {
        View view = new View();
        view.runView();
        view.selectedPlayer.minMax(view, view.playerTwo);
    }
}
