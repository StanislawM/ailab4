import java.awt.*;

public class PlayerTwo extends Player {

    public PlayerTwo() {
        this.playerEnum = PlayerEnum.PLAYER_TWO;
        this.color = Color.BLUE;
    }
}
