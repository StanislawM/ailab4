import java.awt.*;

public class PlayerOne extends Player {

    public PlayerOne() {
        this.playerEnum = PlayerEnum.PLAYER_ONE;
        this.color = Color.RED;
    }
}
